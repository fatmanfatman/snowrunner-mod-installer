﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnowRunnerModIo
{
    public partial class ChooseMode : Form
    {
        public ChooseMode()
        {
            InitializeComponent();
        }

     

        private void buttonModIo_Click(object sender, EventArgs e)
        {
            Hide();
            new InstallSubscribedMods().Show();
        }

        private void buttonStandaloneInstall_Click(object sender, EventArgs e)
        {
            Hide();
            new InstallStandaloneMods().Show();
            
        }
    }
}
