﻿namespace SnowRunnerModIo
{
    partial class ChooseMode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChooseMode));
            this.buttonModIo = new System.Windows.Forms.Button();
            this.buttonStandaloneInstall = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonModIo
            // 
            resources.ApplyResources(this.buttonModIo, "buttonModIo");
            this.buttonModIo.Name = "buttonModIo";
            this.buttonModIo.UseVisualStyleBackColor = true;
            this.buttonModIo.Click += new System.EventHandler(this.buttonModIo_Click);
            // 
            // buttonStandaloneInstall
            // 
            resources.ApplyResources(this.buttonStandaloneInstall, "buttonStandaloneInstall");
            this.buttonStandaloneInstall.Name = "buttonStandaloneInstall";
            this.buttonStandaloneInstall.UseVisualStyleBackColor = true;
            this.buttonStandaloneInstall.Click += new System.EventHandler(this.buttonStandaloneInstall_Click);
            // 
            // ChooseMode
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonStandaloneInstall);
            this.Controls.Add(this.buttonModIo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ChooseMode";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonModIo;
        private System.Windows.Forms.Button buttonStandaloneInstall;
    }
}