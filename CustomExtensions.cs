﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowRunnerModIo
{
    static class CustomExtensions
    {
        public static int getUnixTimestamp(this DateTime ths)
        {
            return (int)ths.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}
