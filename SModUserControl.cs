﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SnowRunnerModIo
{
    public partial class SModUserControl : UserControl
    {
        public SModUserControl()
        {
            InitializeComponent();
        }

        private SMod mod;

        public SMod Mod
        {
            get { return mod; }
            set
            {
                mod = value;
                textBoxName.Text = mod.Name;
                textBoxId.Text = mod.Id.ToString();
                textBoxPath.Text = mod.Folder;
                buttonDisable.Enabled = mod.IsEnabled;
                buttonEnable.Enabled = !mod.IsEnabled;

                labelPak.Text = mod.IsPakInstalled ? "Pak" : "Online";
                if (!mod.IsEnabled)
                {
                    //textBoxPath.ForeColor = textBoxName.ForeColor = textBoxId.ForeColor = Color.White;
                }


            }
        }

        private void SModUserControl_BackColorChanged(object sender, EventArgs e)
        {
            textBoxId.BackColor = textBoxPath.BackColor = textBoxName.BackColor = BackColor;
        }

        private void buttonDisable_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(listBox1.Items[1].ToString(), "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                StandaloneInstall.Instance.SetModState(mod, false);
                this.OnValueChanged(null);
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(listBox1.Items[2].ToString(), "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                StandaloneInstall.Instance.RemoveMod(mod);
                this.OnValueChanged(null);
            }
        }

        private void buttonEnable_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(listBox1.Items[0].ToString(), "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                StandaloneInstall.Instance.SetModState(mod, true);
                this.OnValueChanged(null);
            }
        }

        private EventHandler onValueChanged;
        public event EventHandler ValueChanged
        {
            add
            {
                onValueChanged += value;
            }
            remove
            {
                onValueChanged -= value;
            }
        }
        protected virtual void OnValueChanged(EventArgs e)
        {
            onValueChanged?.Invoke(this, e);
        }

    }
}