﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading;

namespace SnowRunnerModIo
{
    internal class ModIoREST
    {
        private static readonly Lazy<ModIoREST> lazy = new Lazy<ModIoREST>(() => new ModIoREST());
        public static ModIoREST Instance { get { return lazy.Value; } }

        private ModIoREST()
        {
        }

        public const int GAME_ID = 306;

        public List<Mod> GetSubscribedMods()
        {
            Console.WriteLine(Program.Settings);
            var client = new RestClient(Program.Settings.APIBaseURL);
            client.Authenticator = new RestSharp.Authenticators.JwtAuthenticator(Program.Settings.Token);

            var req = new RestRequest("/me/subscribed", Method.GET);
            req.AddHeader("Accept", "application/json");
            req.AddParameter("game_id", GAME_ID);

            var resp = client.Execute<ModData>(req);
            //Console.WriteLine(resp.Content);
            foreach (var item in resp.Data.data)
            {
                Console.WriteLine("{0}      {1}     {2}     {3}", item.id, item.profile_url, item.modfile.download.binary_url, item.modfile.filename);
                item.isInstalledLocal = IsModInstalled(item);
            }

            return resp.Data.data;
        }

        public bool IsModInstalled(Mod mod)
        {
            string path = Program.Settings.WorkFolder;

            string pathModIoFolder = Path.GetFullPath(path + "/../../Mods/.modio/");
            if (!Directory.Exists(pathModIoFolder))
            {
                return false;
            }

            var installedModsFilename = Path.Combine(pathModIoFolder, "installed_mods.json");
            if (!File.Exists(installedModsFilename))
            {
                return false;
            }

            string pathModsFolder = Path.GetFullPath(path + "/../../Mods/.modio/mods/");
            if (!Directory.Exists(pathModsFolder))
            {
                return false;
            }

            if (!Directory.Exists(Path.Combine(pathModsFolder, mod.id.ToString())))
            {
                return false;
            }

            var modioJSONFile = Path.Combine(pathModsFolder, mod.id.ToString(), "modio.json");
            if (!File.Exists(modioJSONFile))
            {
                return false;
            }

            var fileJSON = File.ReadAllText(modioJSONFile);
            //Console.WriteLine(fileJSON);
            var localMod = JsonConvert.DeserializeObject<Mod>(fileJSON);

            if (localMod.id != mod.id)
            {
                return false;
            }

            var zzzz = File.ReadAllText(installedModsFilename);
            if (zzzz.Trim() == "null")
            {
                return false;
            }
            var instMods = JsonConvert.DeserializeObject<List<InstalledMod>>(zzzz);

            if (instMods.Find(x => x.mod_id == mod.id && x.modfile_id == mod.modfile.id) == null)
            {
                return false;
            }

            return true;
        }

        public void InstallMod(Mod mod)
        {
            string path = Program.Settings.WorkFolder;

            string pathModIoFolder = Path.GetFullPath(path + "/../../Mods/.modio/");
            if (!Directory.Exists(pathModIoFolder))
            {
                Directory.CreateDirectory(pathModIoFolder);
            }

            string pathModsFolder = Path.GetFullPath(path + "/../../Mods/.modio/mods/");
            if (!Directory.Exists(pathModsFolder))
            {
                Directory.CreateDirectory(pathModsFolder);
            }

            var modFolder = Path.Combine(pathModsFolder, mod.id.ToString());
            if (!Directory.Exists(modFolder))
            {
                Directory.CreateDirectory(modFolder);
            }

            var modLocalFile = Path.Combine(modFolder, mod.modfile.filename);
            WebClient webClient = new WebClient();
            webClient.DownloadFile(mod.modfile.download.binary_url, modLocalFile);
            var existingPaks = Directory.GetFiles(modFolder, "*.pak");
            foreach (var item in existingPaks)
            {
                File.Delete(Path.Combine(modFolder, item));
            }

            ZipFile.ExtractToDirectory(modLocalFile, modFolder);

            File.Delete(modLocalFile);

            var modLocalInfoFile = Path.Combine(modFolder, "modio.json");
            ModInfo mi = new ModInfo
            {
                date_added = mod.date_added,
                date_live = mod.date_live,
                date_updated = mod.date_updated,
                game_id = mod.game_id,
                id = mod.id,
                name = mod.name,
                name_id = mod.name_id,
                summary = mod.summary,
                visible = 1
            };
            File.WriteAllText(modLocalInfoFile, JsonConvert.SerializeObject(mi));

            var installedModsFilename = Path.Combine(pathModIoFolder, "installed_mods.json");
            List<InstalledMod> instMods = null;
            if (!File.Exists(installedModsFilename) || File.ReadAllText(installedModsFilename).Trim() == "null")
            {
                instMods = new List<InstalledMod>();
            }
            else
            {
                instMods = JsonConvert.DeserializeObject<List<InstalledMod>>(File.ReadAllText(installedModsFilename));
            }

            var existinInstModEntry = instMods.Find(x => x.mod_id == mod.id && x.modfile_id == mod.modfile.id);
            if (existinInstModEntry == null)
            {
                //C:\\Users\\fatman\\Documents\\My Games\\SnowRunnerBeta\\base\\Mods/.modio/mods/110675/
                instMods.Add(
                    new InstalledMod
                    {
                        path = Path.Combine(pathModsFolder, mod.id.ToString()) + "/",
                        mod_id = mod.id,
                        modfile_id = mod.modfile.id,
                        date_updated = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds
                    }
                );
            }
            else
            {
            }

            WriteInstalledModsFile(installedModsFilename, instMods);

            var userProfile = GetModsUserState();

            if (userProfile.UserProfile.modStateList == null)
            {
                userProfile.UserProfile.modStateList = new List<modStateClass>();
            }
            var idx = userProfile.UserProfile.modStateList.FindIndex(x => x.modId == mod.id);
            if (idx >= 0)
            {
                userProfile.UserProfile.modStateList[idx].modState = true;
            }
            else
            {
                userProfile.UserProfile.modStateList.Add(new modStateClass { modId = mod.id, modState = true });
            }

            WriteModStateListFile(userProfile);
        }

        public UserProfileFile GetModsUserState()
        {
            string path = Path.GetFullPath(Program.Settings.WorkFolder);
            var userProfileFile = Path.Combine(path, "user_profile.dat");
            var ret = JsonConvert.DeserializeObject<UserProfileFile>(File.ReadAllText(userProfileFile));
            if (ret == null)
            {
                ret = new UserProfileFile();
            }
            return ret;
        }

        public void WriteModStateListFile(UserProfileFile userProfile)
        {
            string path = Path.GetFullPath(Program.Settings.WorkFolder);
            var userProfileFile = Path.Combine(path, "user_profile.dat");

            if (File.Exists(userProfileFile))
            {
                File.SetAttributes(userProfileFile, File.GetAttributes(userProfileFile) & ~FileAttributes.ReadOnly & ~FileAttributes.System & ~FileAttributes.Hidden & ~FileAttributes.Archive);
            }
            File.WriteAllText(userProfileFile, JsonConvert.SerializeObject(userProfile));
            File.SetAttributes(userProfileFile, File.GetAttributes(userProfileFile) | FileAttributes.ReadOnly | FileAttributes.System | FileAttributes.Hidden | FileAttributes.Archive);
        }

        public void InstallAllMods()
        {
            foreach (var item in GetSubscribedMods())
            {
                Thread.Sleep(1000);

                //InstallMod(item);
            }
        }

        public void RepairInstalledMods()
        {
            string path = Program.Settings.WorkFolder;

            string pathModIoFolder = Path.GetFullPath(path + "/../../Mods/.modio/");
            string pathModsFolder = Path.GetFullPath(path + "/../../Mods/.modio/mods/");

            var installedModsFilename = Path.Combine(pathModIoFolder, "installed_mods.json");
            List<InstalledMod> instMods = null;
            if (!File.Exists(installedModsFilename) || File.ReadAllText(installedModsFilename).Trim() == "null")
            {
                instMods = new List<InstalledMod>();
            }
            else
            {
                instMods = JsonConvert.DeserializeObject<List<InstalledMod>>(File.ReadAllText(installedModsFilename));
            }

            var mods = this.GetSubscribedMods();

            foreach (var mod in mods)
            {
                var existinInstModEntry = instMods.Find(x => x.mod_id == mod.id && x.modfile_id == mod.modfile.id);
                if (existinInstModEntry == null)
                {
                    instMods.Add(
                        new InstalledMod
                        {
                            path = Path.Combine(pathModsFolder, mod.id.ToString()) + "/",
                            mod_id = mod.id,
                            modfile_id = mod.modfile.id,
                            date_updated = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds
                        }
                    );
                }
            }
            WriteInstalledModsFile(installedModsFilename, instMods);
        }

        public void WriteInstalledModsFile(string installedModsFilename, object instMods)
        {
            if (File.Exists(installedModsFilename))
                File.SetAttributes(installedModsFilename, File.GetAttributes(installedModsFilename) & ~FileAttributes.ReadOnly & ~FileAttributes.System & ~FileAttributes.Hidden & ~FileAttributes.Archive);
            File.WriteAllText(installedModsFilename, JsonConvert.SerializeObject(instMods, Formatting.Indented));
            File.SetAttributes(installedModsFilename, File.GetAttributes(installedModsFilename) | FileAttributes.ReadOnly | FileAttributes.System | FileAttributes.Hidden | FileAttributes.Archive);
        }
    }
}